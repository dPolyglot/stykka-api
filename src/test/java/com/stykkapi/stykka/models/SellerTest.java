package com.stykkapi.stykka.models;

import com.stykkapi.stykka.repositories.SellerRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest

class SellerTest {
    Seller seller;

    @Autowired
    SellerRepository sellerRepository;

    @BeforeEach
    void setUp() {
        seller = new Seller();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void shouldCreateNewSeller(){

        seller.setFirstName("Dozie");
        seller.setLastName("Uya");
        seller.setEmail("seller@email.com");
        seller.setPassword("my_password");
        seller.setStoreName("Dozie Enterprises");
        seller.setBankName("Ubi Ego Bank");
        seller.setAccountNumber("1234567898");

        sellerRepository.save(seller);
        assertEquals(1,sellerRepository.count());
    }

    @Test
    void shouldReadSellerDetail(){
        Optional<Seller> findSeller = sellerRepository.findById("607162df99cc1449ca6a9b99");
        assertEquals("Dozie",findSeller.get().getFirstName());
    }

    @Test
    void shouldUpdateSellerDetail(){
        Optional <Seller> updateName = sellerRepository.findById("607162df99cc1449ca6a9b99");
        updateName.get().setFirstName("Dozzy");
        sellerRepository.save(updateName.get());
        assertEquals("Dozzy", updateName.get().getFirstName());
    }

    @Test
    void shouldDeleteSeller(){
        seller.setFirstName("Mr Dozie");
        seller.setLastName("Uya");
        seller.setEmail("Mr.seller@email.com");
        seller.setPassword("my_password");
        seller.setStoreName("Importer Exporter Enterprises");
        seller.setBankName("Money People Bank");
        seller.setAccountNumber("1234567898");

        sellerRepository.save(seller);
        sellerRepository.deleteById("607162df99cc1449ca6a9b99");
        assertEquals(1,sellerRepository.count());
    }

    @Test
    void shouldDeleteAllSeller(){
        sellerRepository.deleteAll();
        assertEquals(0,sellerRepository.count());
    }
}