package com.stykkapi.stykka.services;

import com.stykkapi.stykka.dtos.AddressDTO;
import com.stykkapi.stykka.dtos.RegisterBuyerDTO;
import com.stykkapi.stykka.dtos.UpdateAuthDTO;
import com.stykkapi.stykka.exceptions.BuyerNotFoundException;
import com.stykkapi.stykka.exceptions.EmailExistsException;
import com.stykkapi.stykka.exceptions.InvalidPasswordException;
import com.stykkapi.stykka.models.Address;
import com.stykkapi.stykka.models.Buyer;
import com.stykkapi.stykka.repositories.BuyerRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BuyerServiceImplTest {

    @Autowired
    private BuyerRepository buyerDb;

    @Autowired
    private BuyerService buyerService;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void shouldCreateABuyer(){
        RegisterBuyerDTO newBuyer = new RegisterBuyerDTO();
        newBuyer.setFirstName("Janet");
        newBuyer.setLastName("Jones");
        newBuyer.setEmail("gx@gmail.com");
        newBuyer.setPassword("jones125");

        try{
            buyerService.registerBuyer(newBuyer);
            assertEquals(1, buyerDb.count());
        }catch(EmailExistsException e){
            e.getLocalizedMessage();
        }
    }

    @Test
    void shouldReturnAllBuyersInDatabase(){
        assertEquals(1, buyerService.getAllBuyers().stream().count());
    }

    @Test
    void shouldThrowAnExceptionIfBuyerDoesNotExist(){
       assertThrows(NoSuchElementException.class, () -> buyerService.getOneBuyer( "hjdjjjdjdj"));
    }

    @Test
    void shouldAllowBuyerChangePassword()  {
        UpdateAuthDTO passwordDTO = new UpdateAuthDTO();
        Optional<Buyer> foundBuyer = buyerDb.findByBuyerId("60717609fe01842685280b3d");
        passwordDTO.setOldPassword("234561");
        passwordDTO.setNewPassword("");
        try{
            buyerService.changePassword(passwordDTO,foundBuyer.get().getBuyerId());
        }catch(InvalidPasswordException e){
            System.out.println(e.getLocalizedMessage());
        }
        assertEquals("jones125", foundBuyer.get().getPassword());
    }

    @Test
    void shouldUpdateBuyerDetailIfBuyerExists() {

        Buyer buyer = new Buyer();
        buyer.setFirstName("okay");

        buyerService.updateBuyer(buyer, "60717db0d236e4076663032d");

        assertEquals("okay", buyer.getFirstName());
    }

    @Test
    void shouldAllowBuyerAddNewAddress(){
        Optional<Buyer> existingBuyer = buyerDb.findByBuyerId("60717db0d236e4076663032d");
        AddressDTO buyerAddress = new AddressDTO();
        buyerAddress.setStreetLine("20 oyadiran street");
        buyerAddress.setCity("sabo");
        buyerAddress.setState("lagos");
        buyerAddress.setCountry("nigeria");

        try{
            buyerService.addAddress(buyerAddress, existingBuyer.get().getBuyerId());
            assertEquals(1, existingBuyer.get().getAddresses().size());
        }catch(BuyerNotFoundException e){
            e.getLocalizedMessage();
        }

    }

    @Test
    void shouldEditABuyerAddress(){
        Address updatedAddress = new Address();
        updatedAddress.setStreetLine("29 Oyadiran street");
        String updatedStreetLine = updatedAddress.getStreetLine();
        buyerService.editBuyerAddress(updatedAddress, "6071cd14750c582ca9d89e47");
        assertEquals("29 Oyadiran street", updatedStreetLine);
    }

    @Test
    void shouldDeleteBuyerAddress(){
        Optional<Buyer> existingBuyer = buyerDb.findByBuyerId("60717db0d236e4076663032d");
        try{
            buyerService.deleteAddress("6071cb0402796b7c403c7d9e", existingBuyer.get().getBuyerId());
        }catch(NoSuchElementException e){
            e.getLocalizedMessage();
        }
        assertEquals(0, existingBuyer.get().getAddresses().size());
    }

    @Test
    void shouldDeleteABuyer(){
        Optional<Buyer> buyerToDelete = buyerDb.findByBuyerId("60717609fe01842685280b3d");
        try{
            buyerService.deleteBuyer(buyerToDelete.get().getBuyerId());
        }catch(NoSuchElementException e){
            System.out.println(e.getLocalizedMessage());
        }
        assertEquals(1, buyerDb.count());
    }
}