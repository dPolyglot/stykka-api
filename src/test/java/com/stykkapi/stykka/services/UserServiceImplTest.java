package com.stykkapi.stykka.services;

import com.stykkapi.stykka.dtos.LoginDTO;
import com.stykkapi.stykka.exceptions.BuyerNotFoundException;
import com.stykkapi.stykka.exceptions.InvalidEmailException;
import com.stykkapi.stykka.exceptions.InvalidPasswordException;
import com.stykkapi.stykka.exceptions.SellerException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class UserServiceImplTest {
    @Autowired
    private UserService userService;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testThatAnExistingBuyerCanLogin(){
        LoginDTO existingBuyer = new LoginDTO();
        existingBuyer.setEmail("gx@gmail.com");
        existingBuyer.setPassword("pws");

        try{
            userService.loginBuyer(existingBuyer);
        }catch(InvalidEmailException | InvalidPasswordException | BuyerNotFoundException e){
            e.getLocalizedMessage();
        }
        assertThrows(InvalidEmailException.class, () -> userService.loginBuyer(existingBuyer));
        assertThrows(InvalidPasswordException.class, () -> userService.loginBuyer(existingBuyer));
    }

    @Test
    void testThatAnExistingSellerCanLogin(){
        LoginDTO existingSeller = new LoginDTO();
        existingSeller.setEmail("selleemail@uemail.com");
        existingSeller.setPassword("pws");
        try{
            userService.loginSeller(existingSeller);
        }catch(InvalidEmailException | InvalidPasswordException | SellerException e){
            e.getLocalizedMessage();
        }
        assertThrows(InvalidEmailException.class, () -> userService.loginSeller(existingSeller));
    }
}