package com.stykkapi.stykka.services;

import com.stykkapi.stykka.dtos.LoginDTO;
import com.stykkapi.stykka.exceptions.BuyerNotFoundException;
import com.stykkapi.stykka.exceptions.InvalidEmailException;
import com.stykkapi.stykka.exceptions.InvalidPasswordException;
import com.stykkapi.stykka.exceptions.SellerException;

public interface UserService {
    void loginBuyer(LoginDTO existingUser) throws InvalidPasswordException, InvalidEmailException, BuyerNotFoundException;

    void loginSeller(LoginDTO existingUser) throws InvalidPasswordException, InvalidEmailException, SellerException;
}
