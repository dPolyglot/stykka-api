package com.stykkapi.stykka.services;

import com.stykkapi.stykka.dtos.RegisterSellerDTO;
import com.stykkapi.stykka.exceptions.SellerException;
import com.stykkapi.stykka.models.Seller;
import com.stykkapi.stykka.repositories.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class SellerServiceImpl implements SellerService {

    @Autowired
    private SellerRepository sellerRepository;
    // This should not be void. It should return the newly created seller

    /*
    - update seller
    -
     */
    @Override
    // use a descriptive name for method - registerSeller maybe...
    public Seller saveSeller(RegisterSellerDTO newSeller) throws SellerException{
        Optional<Seller> optionalSeller = sellerRepository.findSellerByEmail(newSeller.getEmail());

        if (optionalSeller.isPresent()){
            throw new SellerException("Seller Already exist");
        }

        Seller seller = new Seller();
        seller.setFirstName(newSeller.getFirstName());
        seller.setLastName(newSeller.getLastName());
        seller.setEmail(newSeller.getEmail());
        seller.setPassword(newSeller.getPassword());
        seller.setStoreName(newSeller.getStoreName());
        seller.setBankName(newSeller.getBankName());
        seller.setAccountNumber(newSeller.getAccountNumber());

        return sellerRepository.save(seller);
    }

    @Override
    public List<Seller> findAllSeller() {
        return sellerRepository.findAll();
    }


    @Override
    public Optional<Seller> findBySellerByName(String storeName) {
        // fix this - you are looking for a store by name not by it's id
        if (sellerRepository.findById(storeName).isPresent()) {
            return findBySellerByName(storeName);
        }else
            throw new NoSuchElementException("Seller not found");
    }

    @Override
    public void deleteBySellerId(String sellerId) throws SellerException {
        if (sellerRepository.findById(sellerId).isPresent()) {
            sellerRepository.deleteBySellerId(sellerId);
        }
        else
            throw new SellerException("No seller found with that Id");
    }

    // Data is important, so we won't do this
//    @Override
//    public void deleteAll(Seller seller) throws SellerException {
//        sellerRepository.deleteAll();
//    }
}
