package com.stykkapi.stykka.services;

import com.stykkapi.stykka.dtos.AddressDTO;
import com.stykkapi.stykka.models.Address;
import com.stykkapi.stykka.repositories.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService{
    @Autowired
    private AddressRepository addressDb;

    @Override
    public Address createAddress(AddressDTO newAddress, String id) {
        Address address = new Address();
        address.setStreetLine(newAddress.getStreetLine());
        address.setCity(newAddress.getCity());
        address.setState(newAddress.getState());
        address.setCountry(newAddress.getCountry());
        address.setUserId(id);
        addressDb.save(address);
        return address;
    }

    @Override
    public Address editAddress(Address editedAddress, String addressId) {
        Optional<Address> foundAddress = Optional.ofNullable(addressDb.findAddressesByAddressId(addressId));
        Address existingAddress = foundAddress.get();

        if(editedAddress.getStreetLine() != null){
            existingAddress.setStreetLine(editedAddress.getStreetLine());
        }

        if(editedAddress.getCity() != null){
            existingAddress.setCity(editedAddress.getCity());
        }

        if(editedAddress.getState() != null){
            existingAddress.setState(editedAddress.getState());
        }

        if(editedAddress.getCountry() != null){
            existingAddress.setCountry(editedAddress.getCountry());
        }

        addressDb.save(existingAddress);
        return existingAddress;
    }

    @Override
    public String deleteAddress(String addressId, String id) {
        Optional<Address> foundAddress = Optional.ofNullable(addressDb.findAddressesByAddressId(addressId));
        Address existingAddress = foundAddress.get();
        String addressToDelete = existingAddress.getAddressId();

        addressDb.deleteAddressByAddressId(existingAddress.getAddressId());
        return addressToDelete;
    }
}
