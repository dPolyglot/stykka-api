package com.stykkapi.stykka.services;

import com.stykkapi.stykka.dtos.AddressDTO;
import com.stykkapi.stykka.dtos.RegisterBuyerDTO;
import com.stykkapi.stykka.dtos.UpdateAuthDTO;
import com.stykkapi.stykka.exceptions.BuyerNotFoundException;
import com.stykkapi.stykka.exceptions.EmailExistsException;
import com.stykkapi.stykka.exceptions.InvalidPasswordException;
import com.stykkapi.stykka.models.Address;
import com.stykkapi.stykka.models.Buyer;

import java.util.List;
import java.util.Optional;

public interface BuyerService {
    Buyer registerBuyer(RegisterBuyerDTO newBuyer) throws EmailExistsException;

    List<Buyer> getAllBuyers();

    Optional<Buyer> getOneBuyer(String buyerId);

    Buyer updateBuyer(Buyer buyerToUpdate, String buyerId);

    void changePassword(UpdateAuthDTO buyerPasswordDTO, String buyerId) throws InvalidPasswordException;

    Optional<Buyer> deleteBuyer(String buyerId);

    void addAddress(AddressDTO addressDTO, String buyerId) throws BuyerNotFoundException;

    void editBuyerAddress(Address editedAddress, String addressId);

    void deleteAddress(String addressId, String buyerId);
}
