package com.stykkapi.stykka.services;

import com.stykkapi.stykka.dtos.AddressDTO;
import com.stykkapi.stykka.dtos.RegisterBuyerDTO;
import com.stykkapi.stykka.dtos.UpdateAuthDTO;
import com.stykkapi.stykka.exceptions.BuyerNotFoundException;
import com.stykkapi.stykka.exceptions.EmailExistsException;
import com.stykkapi.stykka.exceptions.InvalidPasswordException;
import com.stykkapi.stykka.models.Address;
import com.stykkapi.stykka.models.Buyer;
import com.stykkapi.stykka.repositories.AddressRepository;
import com.stykkapi.stykka.repositories.BuyerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class BuyerServiceImpl implements BuyerService{

    @Autowired
    private BuyerRepository buyerDb;

    @Autowired
    private AddressServiceImpl addressService;

    @Autowired
    private AddressRepository addressDb;

    /**
     * This registers new buyers
     * RegisterBuyerDTO - frontend (form for user inputs)
     * Create an instance of the buyer model
     *
     * Use optionalBuyer to check if new buyer email already exists in the database(buyerDb)
     *
     * if buyer email exists throw EmailExistsException else store buyer inputs in database
     * @author  saucekode
     * @param newBuyer
     * @return buyer
     * @version 1.0
     * @since   2021-04-5
     */
    @Override
    public Buyer registerBuyer(RegisterBuyerDTO newBuyer) throws EmailExistsException {
        Buyer buyer = new Buyer();

        Optional<Buyer> optionalBuyer = buyerDb.findBuyerByEmail(newBuyer.getEmail());

        if(optionalBuyer.isPresent()) throw new EmailExistsException("Email already exists");

        buyer.setFirstName(newBuyer.getFirstName());
        buyer.setLastName(newBuyer.getLastName());
        buyer.setEmail(newBuyer.getEmail());
        buyer.setPassword(newBuyer.getPassword());

        saveBuyerToDb(buyer);

        return buyer;
    }

    /**
     * This saves data to repo
     * @param buyer
     * @author saucekode
     * @since 2021-05-3
     */
    private void saveBuyerToDb(Buyer buyer){
        buyerDb.save(buyer);
    }

    /**
     * This gets every buyer from the database
     * @author saucekode
     * @since 2021-05-3
     * @return all buyers
     */
    @Override
    public List<Buyer> getAllBuyers() {
        return buyerDb.findAll();
    }

    /**
     * Get a buyer by their id
     * @param buyerId
     * @return the found buyer
     */
    @Override
    public Optional<Buyer> getOneBuyer(String buyerId){
        if(buyerDb.findByBuyerId(buyerId).isPresent()){
            return buyerDb.findByBuyerId(buyerId);
        }else{
            throw new NoSuchElementException("Buyer with id: " + buyerId + " does not exist");
        }
    }

    /**
     * This finds a buyer in the database and allows them update their data
     * if buyer is not found in db, throw NoSuchElementException found,
     * else update their data and save in db
     * @param buyerToUpdate, buyerId
     * @return the found buyer
     */
    @Override
    public Buyer updateBuyer(Buyer buyerToUpdate, String buyerId){
        Optional<Buyer> foundBuyer = buyerDb.findByBuyerId(buyerId);
//        Buyer existingBuyer = foundBuyer.get();

        foundBuyer.ifPresent(buyer -> {
                    if (buyerToUpdate.getFirstName() != null) {
                        buyer.setFirstName(buyerToUpdate.getFirstName());
                    }

                    if (buyerToUpdate.getLastName() != null) {
                        buyer.setLastName(buyerToUpdate.getLastName());
                    }

                    if (buyerToUpdate.getEmail() != null) {
                        buyer.setEmail(buyerToUpdate.getEmail());
                    }
              }
        );

        saveBuyerToDb(foundBuyer.get());
        return foundBuyer.get();
    }

    /**
     * This updates the password of a buyer
     * Buyer has to provide old password,
     * then the new password can be set
     *
     * if new password is empty on save,
     * set password back to old password
     * @author saucekode
     * @since 2021-04-5
     * @param buyerPasswordDTO, buyerId
     */
    @Override
    public void changePassword(UpdateAuthDTO buyerPasswordDTO, String buyerId) throws InvalidPasswordException {
        Optional<Buyer> foundBuyer = Optional.ofNullable(buyerDb.findByBuyerId(buyerId)
                                        .orElseThrow(() -> new NoSuchElementException("Buyer with id: " + buyerId + " does not exist")));

        String oldPassword = foundBuyer.get().getPassword();

        if(oldPassword.equals(buyerPasswordDTO.getOldPassword())){
            foundBuyer.get().setPassword(buyerPasswordDTO.getNewPassword());

            if(buyerPasswordDTO.getNewPassword().isEmpty()){
                foundBuyer.get().setPassword(oldPassword);
            }
        }else{
            throw new InvalidPasswordException("Provide your old password");
        }
        saveBuyerToDb(foundBuyer.get());
    }


    /**
     * This deletes a buyer by id
     * @param buyerId
     * @author saucekode
     * @return delete buyer from database
     */
    @Override
    public Optional<Buyer> deleteBuyer(String buyerId){
        if(buyerDb.findByBuyerId(buyerId).isPresent()){
           return buyerDb.deleteBuyerByBuyerId(buyerId);
        }else{
            throw new NoSuchElementException("Buyer does not exist");
        }
    }

    /**
     *  This adds an existing address to the buyer address hashset
     * @param buyerId
     * @return buyer
     * @author saucekode
     * @since 2021-06-4
     */
    @Override
    public void addAddress(AddressDTO addressDTO, String buyerId) throws BuyerNotFoundException {

        Optional<Buyer> optionalBuyer = buyerDb.findByBuyerId(buyerId);

        if(optionalBuyer.isEmpty()){
            throw new BuyerNotFoundException("Buyer not found");
        }else{
            Buyer buyer = optionalBuyer.get();
            Address createdAddress = addressService.createAddress(addressDTO, buyerId);
            HashSet<Address> addresses = buyer.getAddresses();
            addresses.add(createdAddress);

            buyer.setAddresses(addresses);
            buyerDb.save(buyer);
        }
    }

    /**
     * This finds the address by id from the address database,
     * and finds the buyer by id from the buyer database.
     * Update the address in the address db and save
     * Loop through the addresses hashset and check,
     * if the address id in the addressdb matches that in the buyer hashset
     * remove the address from the buyer hashset and add in the updated address
     * save back to the buyerdb
     * @param editedAddress, addressId, buyerId
     */
    @Override
    public void editBuyerAddress(Address editedAddress, String addressId) {
        Optional<Address> optionalAddress = Optional.ofNullable(addressDb.findAddressesByAddressId(addressId));
        String buyerId = optionalAddress.get().getUserId();

        Optional<Buyer> existingBuyer = buyerDb.findByBuyerId(buyerId);
        Buyer getExistingBuyer = existingBuyer.get();
        HashSet<Address> buyerAddresses = getExistingBuyer.getAddresses();

        Address existingAddress = addressService.editAddress(editedAddress, addressId);

        buyerAddresses.removeIf(addresses -> addresses.getAddressId().equals(existingAddress.getAddressId()));
        buyerAddresses.add(existingAddress);
        saveBuyerToDb(getExistingBuyer);
    }

    /**
     * This deletes the address from the addressdb and buyer hashset
     * @param addressId
     * @param id
     */
    @Override
    public void deleteAddress(String addressId, String id) {

        Optional<Buyer> foundBuyer = buyerDb.findByBuyerId(id);
        Buyer existingBuyer = foundBuyer.get();
        HashSet<Address> allBuyerAddresses = existingBuyer.getAddresses();

        String deletedAddress = addressService.deleteAddress(addressId, id);
        allBuyerAddresses.removeIf(address -> address.getAddressId().equals(deletedAddress));
        saveBuyerToDb(existingBuyer);
    }

}
