package com.stykkapi.stykka.services;

import com.stykkapi.stykka.dtos.AddressDTO;
import com.stykkapi.stykka.models.Address;

public interface AddressService {
    Address createAddress(AddressDTO newAddress, String id);

    Address editAddress(Address editedAddress, String addressId);

    String deleteAddress(String addressId, String id);
}
