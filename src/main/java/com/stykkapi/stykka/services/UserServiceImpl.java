package com.stykkapi.stykka.services;

import com.stykkapi.stykka.dtos.LoginDTO;
import com.stykkapi.stykka.exceptions.BuyerNotFoundException;
import com.stykkapi.stykka.exceptions.InvalidEmailException;
import com.stykkapi.stykka.exceptions.InvalidPasswordException;
import com.stykkapi.stykka.exceptions.SellerException;
import com.stykkapi.stykka.models.Buyer;
import com.stykkapi.stykka.models.Seller;
import com.stykkapi.stykka.repositories.BuyerRepository;
import com.stykkapi.stykka.repositories.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private SellerRepository sellerDb;

    @Autowired
    private BuyerRepository buyerDb;

    /**
     * This allows an existent buyer login
     * @param existingUser
     */
    @Override
    public void loginBuyer(LoginDTO existingUser) throws InvalidPasswordException, InvalidEmailException, BuyerNotFoundException {
        Optional<Buyer> foundBuyerEmail = Optional.ofNullable(buyerDb.findBuyerByEmail(existingUser.getEmail())
                .orElseThrow(() -> new BuyerNotFoundException("Invalid email or password")));
        String existingBuyerPassword = foundBuyerEmail.get().getPassword();

        if(foundBuyerEmail.isEmpty()){
            throw new InvalidEmailException("Invalid email");
        }

        if(!(existingBuyerPassword.equals(existingUser.getPassword()))){
            throw new InvalidPasswordException("Invalid password");
        }
    }

    /**
     * This allows an existent seller login
     * @param existingUser
     */
    @Override
    public void loginSeller(LoginDTO existingUser) throws InvalidPasswordException, InvalidEmailException, SellerException {
        Optional<Seller> foundSellerEmail = Optional.of(sellerDb.findSellerByEmail(existingUser.getEmail())
                                            .orElseThrow(() -> new SellerException("Invalid email or password")));
        String existingSellerPassword = foundSellerEmail.get().getPassword();

        if(foundSellerEmail.isEmpty()){
            throw new InvalidEmailException("Invalid email");
        }

        if(!(existingSellerPassword.equals(existingUser.getPassword()))){
            throw new InvalidPasswordException("Invalid password");
        }
    }
}
