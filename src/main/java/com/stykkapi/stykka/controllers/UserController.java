package com.stykkapi.stykka.controllers;

import com.stykkapi.stykka.dtos.LoginDTO;
import com.stykkapi.stykka.exceptions.BuyerNotFoundException;
import com.stykkapi.stykka.exceptions.InvalidEmailException;
import com.stykkapi.stykka.exceptions.InvalidPasswordException;
import com.stykkapi.stykka.exceptions.SellerException;
import com.stykkapi.stykka.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/")
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping(value = "login")
    public ResponseEntity<?> buyerLogin(@RequestBody LoginDTO existingUser){
        try {
            userService.loginBuyer(existingUser);
        } catch (InvalidPasswordException | InvalidEmailException | BuyerNotFoundException e) {
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.ALREADY_REPORTED);
        }
        return new ResponseEntity<>( "Login successful", HttpStatus.OK);
    }

    @GetMapping(value = "seller/login")
    public ResponseEntity<?> sellerLogin(@RequestBody LoginDTO existingUser){
        try {
            userService.loginSeller(existingUser);
        } catch (InvalidPasswordException | InvalidEmailException | SellerException e) {
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.ALREADY_REPORTED);
        }
        return new ResponseEntity<>( "Login successful", HttpStatus.OK);
    }
}
