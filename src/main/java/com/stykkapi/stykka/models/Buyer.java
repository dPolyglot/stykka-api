package com.stykkapi.stykka.models;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("buyers")
//@NoArgsConstructor
public class Buyer extends User{
    @Id
    private String buyerId;
}
