package com.stykkapi.stykka.models;

import lombok.Data;

import java.util.HashSet;

@Data
public class User {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String newPassword;
    private HashSet<Address> addresses = new HashSet<>();
}
