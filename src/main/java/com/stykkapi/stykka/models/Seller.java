package com.stykkapi.stykka.models;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "sellers")
@NoArgsConstructor
public class Seller extends User{
    @Id
    private String sellerId;
    private String storeName;
    private String bankName;
    private String accountNumber;
//    private List<Product> listOfProduct;
}
