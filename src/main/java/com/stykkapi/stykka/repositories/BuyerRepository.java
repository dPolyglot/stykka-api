package com.stykkapi.stykka.repositories;

import com.stykkapi.stykka.models.Buyer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BuyerRepository extends MongoRepository<Buyer, String> {
    Optional<Buyer> findBuyerByEmail(String email);

    Optional<Buyer> findByBuyerId(String buyerId);

    Optional<Buyer> deleteBuyerByBuyerId(String buyerId);

    Optional<Buyer> findBuyerByPassword(String password);

}
