package com.stykkapi.stykka.repositories;

import com.stykkapi.stykka.models.Address;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends MongoRepository<Address, String>{

    Address findAddressesByAddressId(String addressId);

    void deleteAddressByAddressId(String addressId);
}
