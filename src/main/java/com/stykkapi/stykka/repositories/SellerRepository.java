package com.stykkapi.stykka.repositories;

import com.stykkapi.stykka.models.Seller;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SellerRepository extends MongoRepository<Seller, String> {

    void deleteBySellerId(String sellerId);

    Optional<Seller> findSellerByEmail(String email);

    Optional<Seller> findSellerByPassword(String password);
}
