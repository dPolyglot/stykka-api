package com.stykkapi.stykka.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UpdateAuthDTO {
    @NotNull
    private String oldPassword;
    @NotNull
    private String newPassword;
}
